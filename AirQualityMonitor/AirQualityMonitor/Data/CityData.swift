//
//  StateData.swift
//  AirQualityMonitor
//
//  Created by Shweta Sawant on 13/02/22.
//

import Foundation

struct CityData: Codable {
    var city:String
    var aqi:Double
    
}

struct CityDisplayObject {
    var city:String
    var aqi:Double
    var updatedTime:Date
}

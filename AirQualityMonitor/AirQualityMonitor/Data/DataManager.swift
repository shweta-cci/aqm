//
//  DataManager.swift
//  AirQualityMonitor
//
//  Created by Shweta Sawant on 13/02/22.
//

import Foundation

protocol DataManagerProtocol {
    func updateUIWithNewData(_ cityDetails: Array<CityDisplayObject>)
}

class DataManager: NSObject {
    
    public static let sharedInstance = DataManager()
    let socketManager = SocketManager()
    var delegate: DataManagerProtocol?
    var cityDetails: Array<CityDisplayObject> = []
    var last20Records: [String : Array<Double>] = [:]
    
    override init() {
        super.init()
        socketManager.setupSocket("ws://city-ws.herokuapp.com/")
        socketManager.delegate = self
    }
    
    func getCityDetails() {
        socketManager.connectSocket()
    }
    
    func getLast20PointsFor(city: String) -> Array<Double> {
        return last20Records[city] ?? []
    }
    
    func updateCityData(_ fetchedData: Array<CityData>) {
        
        if cityDetails.count == 0 {
            cityDetails = fetchedData.map({ data in
                CityDisplayObject(city: data.city, aqi: data.aqi, updatedTime: Date())
            })
            populateLast20Records()
            delegate?.updateUIWithNewData(cityDetails)
            return
        }
        
        let entriesPresent = fetchedData.filter({cityDetails.map{$0.city}.contains($0.city)})
        
        var updatedEntries = cityDetails.map { data in
            CityDisplayObject(city: data.city, aqi: entriesPresent.filter({$0.city == data.city}).first?.aqi ?? data.aqi, updatedTime: Date())
        }
        
        let missingEntries = fetchedData.filter({!entriesPresent.map{$0.city}.contains($0.city)})
        updatedEntries.append(contentsOf: missingEntries.map({ data in
            CityDisplayObject(city: data.city, aqi: data.aqi, updatedTime: cityDetails.filter({$0.city == data.city}).first?.updatedTime ?? Date())
        }))
        
        cityDetails = updatedEntries
        cityDetails.sort { lhs, rhs in
            return lhs.city < rhs.city
        }
        
        populateLast20Records()
        
        delegate?.updateUIWithNewData(cityDetails)
    }
    
    func populateLast20Records(){
        for cityData in cityDetails {
            if var valuesArray = last20Records[cityData.city]{
                valuesArray.append(cityData.aqi)
                last20Records[cityData.city] = valuesArray
            }else{
                last20Records[cityData.city] = [cityData.aqi]
            }
        }
    }
}

extension DataManager: SocketDelegate {
    func newDataFetched(_ cityDetails: String) {
        
        if let jsonData = cityDetails.data(using: .utf8) {
            do{
                 let decodedData = try JSONDecoder().decode(Array<CityData>.self,
                                                                       from: jsonData)
                updateCityData(decodedData)
            }
            catch{
                print("Error : \(error) ")
            }
        }
    }
}

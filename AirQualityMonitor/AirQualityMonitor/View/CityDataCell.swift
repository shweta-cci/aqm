//
//  CityDataCell.swift
//  AirQualityMonitor
//
//  Created by Shweta Sawant on 13/02/22.
//

import UIKit

class CityDataCell: UITableViewCell {

    @IBOutlet weak var stateName: UILabel!
    @IBOutlet weak var lastUpdatedTime: UILabel!
    
    @IBOutlet weak var AQValue: UILabel!
    @IBOutlet weak var aqBackgroundView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    func setData(_ cityData: CityDisplayObject) {
        stateName.text = cityData.city
        AQValue.text = String(format: "%.2f", cityData.aqi)
        setCategoryColor(cityData.aqi)
        setUpdatedTime(cityData.updatedTime)
    }
    
    
    func setCategoryColor(_ value: Double) {
        switch value {
            case 0..<51:
                aqBackgroundView.backgroundColor =  UIColor(named: "good")
            case 51..<101:
                aqBackgroundView.backgroundColor =  UIColor(named: "satisfactory")
            case 101..<201:
                aqBackgroundView.backgroundColor =  UIColor(named: "moderate")
            case 201..<301:
                aqBackgroundView.backgroundColor =  UIColor(named: "poor")
            case 301..<401:
                aqBackgroundView.backgroundColor = UIColor(named: "verypoor")
            case 401..<501:
                aqBackgroundView.backgroundColor = UIColor(named: "severe")
            default:
                aqBackgroundView.backgroundColor = UIColor(red: 50.0, green: 205.0, blue: 50, alpha: 1.0)
        }
    }
    
    func setUpdatedTime(_ time:Date) {
        if Calendar.current.isDateInToday(time) {
            let minutes =  Date().timeIntervalSince(time)/60
            
            if minutes >= 60 {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "hh:mm a"
                lastUpdatedTime.text = dateFormatter.string(from: time)
            }else if minutes/60 <= 45 {
                lastUpdatedTime.text = "A few seconds ago"
            }else if minutes <= 1.5{
                lastUpdatedTime.text = "A minute ago"
            }else{
                lastUpdatedTime.text = "\(minutes) minutes ago"
            }
            
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YY, MMM d, hh:mm"
            lastUpdatedTime.text = dateFormatter.string(from: time)
        }
    }
}

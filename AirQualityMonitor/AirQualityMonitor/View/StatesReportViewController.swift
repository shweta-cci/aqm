//
//  StatesReportViewController.swift
//  AirQualityMonitor
//
//  Created by Shweta Sawant on 13/02/22.
//

import UIKit

class StatesReportViewController: UIViewController {

    @IBOutlet weak var aqValuesTable: UITableView!
    
    var statesReportViewModel = StatesReportViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        aqValuesTable.estimatedRowHeight = 350.0
        aqValuesTable.tableFooterView = nil
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        statesReportViewModel.delegate = self
        
        
    }

    override func viewDidLayoutSubviews() {
        super .viewDidLayoutSubviews()
    }
 
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath = aqValuesTable.indexPath(for: sender as! CityDataCell)
        let cityDetails = segue.destination as! CityGraphViewController
        let selectedCity = statesReportViewModel.getCityAtIndex(indexPath!.row)
        cityDetails.cityGraphViewModel.selectedCity = selectedCity.city
        
    }


}

extension StatesReportViewController: StatesReportViewModelProtocol, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statesReportViewModel.getCitiesCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cityCell = tableView.dequeueReusableCell(withIdentifier: "CityDataCell") as! CityDataCell
        
        let cityData = statesReportViewModel.getCityAtIndex(indexPath.row)
        cityCell.setData(cityData)
        return cityCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension

    }
    
    func reloadData() {
        aqValuesTable.reloadData()
    }
    
}

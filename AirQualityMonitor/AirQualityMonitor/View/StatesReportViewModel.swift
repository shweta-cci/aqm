//
//  StatesReportViewModel.swift
//  AirQualityMonitor
//
//  Created by Shweta Sawant on 14/02/22.
//

import Foundation

protocol StatesReportViewModelProtocol {
    func reloadData()
}
class StatesReportViewModel {
    
    let dataManager = DataManager.sharedInstance

    var citiesData:Array<CityDisplayObject> = []

    var delegate: StatesReportViewModelProtocol?
    
    init() {
        dataManager.delegate = self
        dataManager.getCityDetails()
    }
    
    func getCitiesCount() -> Int {
        return citiesData.count
    }
    
    func getCityAtIndex(_ index: Int) -> CityDisplayObject {
        return citiesData[index]
    }
    
}


extension StatesReportViewModel: DataManagerProtocol{
    func updateUIWithNewData(_ cityDetails: Array<CityDisplayObject>) {
        citiesData = cityDetails
        delegate?.reloadData()
    }
    
    
}

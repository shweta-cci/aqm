//
//  CityGraphViewController.swift
//  AirQualityMonitor
//
//  Created by Shweta Sawant on 14/02/22.
//

import UIKit
import CorePlot

class CityGraphViewController: UIViewController {

    @IBOutlet weak var graphView: CPTGraphHostingView!
    
    var cityGraphViewModel = CityGraphViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        cityGraphViewModel.getDataPoints()
        configureGraphtView()

    }
    func configureGraphtView(){
        
        graphView.allowPinchScaling = false
        cityGraphViewModel.dataPoints.removeAll()
//        self.currentIndex = 0
                
        //Configure graph
        let graph = CPTXYGraph(frame: graphView.bounds)
        graph.plotAreaFrame?.masksToBorder = false
        graphView.hostedGraph = graph
        graph.backgroundColor = UIColor.black.cgColor
        graph.paddingBottom = 40.0
        graph.paddingLeft = 40.0
        graph.paddingTop = 30.0
        graph.paddingRight = 15.0
        
        //Style for graph title
        let titleStyle = CPTMutableTextStyle()
        titleStyle.color = CPTColor.white()
        titleStyle.fontName = "HelveticaNeue-Bold"
        titleStyle.fontSize = 20.0
        titleStyle.textAlignment = .center
        graph.titleTextStyle = titleStyle

        //Set graph title
        let title = cityGraphViewModel.selectedCity
        graph.title = title
        graph.titlePlotAreaFrameAnchor = .top
        graph.titleDisplacement = CGPoint(x: 0.0, y: 0.0)
        
        let axisSet = graph.axisSet as! CPTXYAxisSet
                
        let axisTextStyle = CPTMutableTextStyle()
        axisTextStyle.color = CPTColor.white()
        axisTextStyle.fontName = "HelveticaNeue-Bold"
        axisTextStyle.fontSize = 10.0
        axisTextStyle.textAlignment = .center
        let lineStyle = CPTMutableLineStyle()
        lineStyle.lineColor = CPTColor.white()
        lineStyle.lineWidth = 5
        let gridLineStyle = CPTMutableLineStyle()
        gridLineStyle.lineColor = CPTColor.gray()
        gridLineStyle.lineWidth = 0.5


        if let x = axisSet.xAxis {
            x.majorIntervalLength   = 30
            x.minorTicksPerInterval = 5
            x.labelTextStyle = axisTextStyle
            x.minorGridLineStyle = gridLineStyle
            x.axisLineStyle = lineStyle
            x.axisConstraints = CPTConstraints(lowerOffset: 0.0)
            x.delegate = self
        }

        if let y = axisSet.yAxis {
            y.majorIntervalLength   = 100
            y.minorTicksPerInterval = 50
            y.minorGridLineStyle = gridLineStyle
            y.labelTextStyle = axisTextStyle
            y.alternatingBandFills = [CPTFill(color: CPTColor.init(componentRed: 255, green: 255, blue: 255, alpha: 0.03)),CPTFill(color: CPTColor.black())]
            y.axisLineStyle = lineStyle
            y.axisConstraints = CPTConstraints(lowerOffset: 0.0)
            y.delegate = self
        }
        
        let xMin = 0.0
        let xMax = 150.0
        let yMin = 0.0
        let yMax = 500.0
        guard let plotSpace = graph.defaultPlotSpace as? CPTXYPlotSpace else { return }
        plotSpace.xRange = CPTPlotRange(locationDecimal: CPTDecimalFromDouble(xMin), lengthDecimal: CPTDecimalFromDouble(xMax - xMin))
        plotSpace.yRange = CPTPlotRange(locationDecimal: CPTDecimalFromDouble(yMin), lengthDecimal: CPTDecimalFromDouble(yMax - yMin))
        
        configurePlot()
    }
    
    func configurePlot(){
        let plot = CPTScatterPlot()
          let plotLineStile = CPTMutableLineStyle()
          plotLineStile.lineJoin = .round
          plotLineStile.lineCap = .round
          plotLineStile.lineWidth = 2
          plotLineStile.lineColor = CPTColor.white()
          plot.dataLineStyle = plotLineStile
          plot.curvedInterpolationOption = .catmullCustomAlpha
          plot.interpolation = .curved
          plot.identifier = "coreplot-graph" as NSCoding & NSCopying & NSObjectProtocol
          guard let graph = graphView.hostedGraph else { return }
          plot.dataSource = self // (self as CPTPlotDataSource)
          plot.delegate = self //(self as CALayerDelegate)
          graph.add(plot, to: graph.defaultPlotSpace)
      }
}

extension CityGraphViewController: CPTScatterPlotDataSource, CPTScatterPlotDelegate {
    func numberOfRecords(for plot: CPTPlot) -> UInt {
        return UInt(cityGraphViewModel.getDataPoints().count)
    }
    
    func number(for plot: CPTPlot, field fieldEnum: UInt, record idx: UInt) -> Any? {
        switch CPTScatterPlotField(rawValue: Int(fieldEnum)) {
            case .X:
                print("Index X \(idx*30)")
                return idx*30
            case .Y:
                print("Index Y \(cityGraphViewModel.getDataPoints()[Int(idx)])")
                return cityGraphViewModel.getDataPoints()[Int(idx)]
            default:
                return 0
        }
    }
}

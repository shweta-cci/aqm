//
//  CityGraphViewModel.swift
//  AirQualityMonitor
//
//  Created by Shweta Sawant on 14/02/22.
//

import Foundation

protocol CityGraphViewModelDelegate {
    func plot(_ array: Array<Int>)
}


class CityGraphViewModel {
    var selectedCity:String = ""
    var dataPoints:Array<Double> = []
    var delegate: CityGraphViewModelDelegate?
    
    var dataManager = DataManager.sharedInstance
    
    init() {
        dataManager.delegate = self
    }
    
    func getDataPoints() -> Array<Double>  {
        dataPoints = dataManager.getLast20PointsFor(city: selectedCity)
        return dataPoints
    }
}

extension CityGraphViewModel: DataManagerProtocol{
    func updateUIWithNewData(_ cityDetails: Array<CityDisplayObject>) {
        
        if let cityData = cityDetails.filter({$0.city == selectedCity}).first {
            dataPoints.append(cityData.aqi)
        }
    }
    
    
}

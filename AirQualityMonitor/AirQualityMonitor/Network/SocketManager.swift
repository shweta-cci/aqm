//
//  SocketManager.swift
//  AirQualityMonitor
//
//  Created by Shweta Sawant on 13/02/22.
//

import Foundation
import Starscream

protocol SocketDelegate {
    func newDataFetched(_ cityDetails: String)
}

class SocketManager {
        
    var socket: WebSocket?
    var delegate:SocketDelegate?
    
    func setupSocket(_ url: String) {
        socket = WebSocket(request: URLRequest(url: URL(string: url)!))
        socket!.delegate = self
        
    }
    
    func connectSocket() {
        socket!.connect()
    }
    func isSocketConnected() -> Bool {
        return true
    }
}

extension SocketManager: WebSocketDelegate {
    
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
            case .connected(let headers):
//                isConnected = true
                print("websocket is connected: \(headers)")

            case .disconnected(let reason, let code):
//                isConnected = false
                print("websocket is disconnected: \(reason) with code: \(code)")
            case .text(let string):
                print("Received text: \(string)")
                delegate?.newDataFetched(string)
            case .binary(let data):
                print("Received data: \(data.count)")
            case .ping(_):
                break
            case .pong(_):
                break
            case .viabilityChanged(_):
                break
            case .reconnectSuggested(_):
                break
            case .cancelled:
                print("error")
//                isConnected = false
            case .error(let error):
//                isConnected = false
//                handleError(error)
                print("error \(error)")
            }
    }
    
    
}

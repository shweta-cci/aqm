# README #

This README would normally document whatever steps are necessary to get your application up and running.

Network:
SocketManager: has calls to connect to socket and handles incoming messages.

Datamanager:
Handles the messages received from Socket Manager and sends data to requesting View Models. The data manager handles all the entries. It has two main variables cityDetails (holds all cities data to display) and last20Records(to hold last 20 points to plot for each city).

Used MVVM Design: 
Each view controller has its own View Model which interacts with the DataManager

I was able to achieve all this in around 10 hrs. 

![AQList](AQ.png)
![Graph](graph.png)



